# U-Boot: Rock Pi 4C based on PKGBUILD for RockPro64
# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich 
# Contributor: Adam <adam900710@gmail.com>
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-orangepi4-lts
pkgver=2023.01
pkgrel=2
_tfaver=2.8
pkgdesc="U-Boot for OrangePi 4 LTS"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('git' 'arm-none-eabi-gcc' 'dtc' 'bc' 'python-setuptools' 'swig')
provides=('uboot')
conflicts=('uboot')
replaces=('uboot-rockpi4')
install=${pkgname}.install
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-${_tfaver}.tar.gz"
        "0001-mmc-sdhci-allow-disabling-sdma-in-spl.patch"    # From list: https://patchwork.ozlabs.org/project/uboot/patch/20220222013131.3114990-3-pgwipeout@gmail.com/
	"0002-add-arm64-dts-rockchip-orangepi-4-lts.patch")
sha256sums=('69423bad380f89a0916636e89e6dcbd2e4512d584308d922d1039d1e4331950f'
            'df4e0f3803479df0ea4cbf3330b59731bc2efc2112c951f9adb3685229163af9'
            '7014c3f1ada93536787a4ce30b484dfe651c339391bd46869c61933825a0edcc'
            '48a737548fab5e95471ce9ae58becbb1351058f9b5ce8cb12c212de77bedb511')

prepare() {
  cd u-boot-${pkgver/rc/-rc}

  patch -N -p1 -i "${srcdir}/0001-mmc-sdhci-allow-disabling-sdma-in-spl.patch"  	# RK3399 suspend/resume
  patch -N -p1 -i "${srcdir}/0002-add-arm64-dts-rockchip-orangepi-4-lts.patch"		# OrangePi 4 LTS support
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd trusted-firmware-a-${_tfaver}

  echo -e "\nBuilding TF-A for OrangePi 4 LTS...\n"
  make PLAT=rk3399
  cp build/rk3399/release/bl31/bl31.elf ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for OrangePi 4 LTS...\n"
  make orangepi_4_lts_rk3399_defconfig
  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_OF_LIBFDT_OVERLAY' 'y'
  update_config 'CONFIG_SPL_MMC_SDHCI_SDMA' 'n'
  update_config 'CONFIG_MMC_SDHCI_SDMA' 'y'
  update_config 'CONFIG_MMC_SPEED_MODE_SET' 'y'
  update_config 'CONFIG_MMC_IO_VOLTAGE' 'y'
  update_config 'CONFIG_MMC_UHS_SUPPORT' 'y'
  update_config 'CONFIG_MMC_HS400_ES_SUPPORT' 'y'
  update_config 'CONFIG_MMC_HS400_SUPPORT' 'y'
  update_config 'CONFIG_SYS_LOAD_ADDR' '0x800800'
  update_config 'CONFIG_TEXT_BASE' '0x00200000'
  update_config 'CONFIG_SPL_HAS_BSS_LINKER_SECTION' 'y'
  update_config 'CONFIG_SPL_BSS_START_ADDR' '0x400000'
  update_config 'CONFIG_SPL_BSS_MAX_SIZE' '0x2000'
  update_config 'CONFIG_HAS_CUSTOM_SYS_INIT_SP_ADDR' 'y'
  update_config 'CONFIG_CUSTOM_SYS_INIT_SP_ADDR' '0x300000'

  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  mkdir -p "${pkgdir}/boot/extlinux"
  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
}
